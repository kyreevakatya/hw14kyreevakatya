@extends('layout')

@section('title', 'Homepage')

@section('body')
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="list-group">
                    <a href="/categories" class="list-group-item list-group-item-action">Categories</a>
                    <a href="/posts" class="list-group-item list-group-item-action">Posts</a>
                    <a href="/tags" class="list-group-item list-group-item-action">Tags</a>
                </div>
            </div>
        </div>
    </div>
@endsection
