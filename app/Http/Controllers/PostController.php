<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('posts.index', ['posts' => $posts]);
    }

    public function form()
    {
        $request = request();
        $categories = Category::all();
        $tags = Tag::all();
        $data = [];

        if ($request->method() == 'POST') {
            $post = Post::updateOrCreate(
                ['id' => $request->get('id')],
                [
                    'title' => $request->get('title'),
                    'slug' => $request->get('slug'),
                    'body' => $request->get('body'),
                    'category_id' => $request->get('category_id')
                ]
            );
            $post->tags()->sync($request->get('tags'));

            return redirect('/posts');
        }

        $data['post'] = null;
        if (!empty($id = $request->route()->parameter('id'))) {
            $data['post'] = Post::find($id);
        }

        return view('posts.form', ['post' => $data['post'], 'categories' => $categories, 'tags' => $tags]);
    }


    public function delete()
    {
        $post = Post::find(request()->route()->parameter('id'));
        $post->tags()->detach();
        $post->delete();

        return redirect('/posts');
    }
}
